﻿using System;

namespace exercise_20
{
    class Program
    {
        static void Main(string[] args)
        {
           //Start the program with Clear();
           Console.Clear();
           Console.WriteLine("Enter 2 numbers,please");
           Console.WriteLine("Enter the 1st one,please");
            
           var a= int.Parse(Console.ReadLine());
           Console.WriteLine("Enter the 2nd one,please");
           var b=int.Parse(Console.ReadLine());
           if(a == b)
           {
               Console.WriteLine("a equals b");
           }
           else if (a>b)
           {
               Console.WriteLine("a is bigger then b");
           }
           else
           {
               Console.WriteLine("a is smaller then b"); 
           }
           

           Console.ReadKey();


           
           var password = "";

           Console.WriteLine("Please type in a password at least 8 characters");
           password=Console.ReadLine();
                     
           if (password.Length<8)
          {
            Console.WriteLine("The password is too short,it must be at least 8 characters long");
          }
          else 
          {
              Console.WriteLine("Complete");

          }


          Console.ReadKey();

          var answer="";
          Console.WriteLine("do you want to change the passcode?'yes'' or 'no'");
         answer = Console.ReadLine();
         if (answer is "yes")
         {
            Console.WriteLine("Type in a password with at least 8 characters ");
            var newPassword="";
            newPassword= Console.ReadLine();
            
            if (newPassword == password)
            {
                Console.WriteLine("You have used this password befor");
            }
            else if (newPassword.Length<8)
         {
            Console.WriteLine("the password is too short");
         }
            else         
            {
                newPassword=password.Replace(password,newPassword);
                Console.WriteLine($"Your current password is{newPassword}");
                
            }
        }
                   

           //End the program with blank line and instructions
           Console.ResetColor();
           Console.WriteLine();
           Console.WriteLine("Press <Enter> to quit the program");
           Console.ReadKey();
        }
    }
}
